### What is this repository for? ###

* Quick summary: This javascript App is made for the ZHAW MLAIT Course by Niclas Simmler
* Version: 1.0

### How do I get set up? ###

* Download all files (3 in total)
* Open the index.html file with your browser (preferably Chrome)
* Open the javascript console and see the output
