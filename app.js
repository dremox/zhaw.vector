Vector.Application = function(){
    return {
        modulo: 0,

        /**
         * Sets the modulo factor
         * @param modulo
         */
        setModulo: function(modulo){
            this.modulo = modulo;
        },

        /**
         * Check Vectors if the scalar 0 is the only possibility to reach Vector [0,0]
         * @param v
         * @param w
         * @returns {string} Returns either the linear combination which also evaluates to [0,0] or Vectors are free
         */
        evalVectors: function(v, w){
            var vData = v.getVector().slice(0);
            var wData = w.getVector().slice(0);

            var check = 0;
            if(vData.length === wData.length){
                for (var i = 1; i < this.modulo; i++) {
                    for (var j = 1; j < this.modulo; j++) {
                        if((+i * +vData[0] + +j* +wData[0])%this.modulo === 0){
                            if((+i * +vData[1] + +j* +wData[1])%this.modulo === 0) {
                                check++;
                            }
                        }
                    }
                }
            }else{
                throw "Vectors mismatch in length!";
            }


            if(check != 0){
                return "Not free Vectors: " + i + "*(" + vData.toString() + ")+" + j + "*(" + wData.toString() + ")=(" + [0,0].toString() + ")";
            }else{

                return "Vectors are free!";
            }
        }
    }
}