var Vector = function(){
    return {
        vector: [],

        /**
         * Returns the vector as Array
         * @returns {Array}
         */
        getVector: function(){
            return this.vector;
        },

        /**
         * Sets the vector for this object
         * @param vector A two-dimensional Array
         */
        setVector: function(vector){
            this.vector = vector;
        }
    }
}